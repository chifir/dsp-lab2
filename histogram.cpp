#include "histogram.h"
#include "ui_histogram.h"

HistoGram::HistoGram(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HistoGram)
{
    ui->setupUi(this);
    connect(ui->pbProccessHist, SIGNAL(clicked(bool)), this, SLOT(onHistProccess()));
}

HistoGram::~HistoGram()
{
    delete ui;
}

void HistoGram::setSourceImage(QImage *srcImg)
{
    this->srcImg = srcImg;
}

void HistoGram::setProccessedImage(QImage *proccessedImg)
{
    this->proccessedImg = proccessedImg;
}

int HistoGram::findMax(int *arr, int size)
{
    int max = arr[0];
    for (int i = 1; i < size; i++)
    {
        if (max < arr[i])
        {
            max = arr[i];
        }
    }

    return max;
}

//void HistoGram::onHistProccess()
//{

//   int outSrc;
//   int outProc;
//   QRgb rgbSrc;
//   QRgb rgbProc;
//   int alphaSrc[256];
//   int alphaProc[256];

//    for(int a = 0; a < 256; a++)
//    {
//        alphaSrc[a] = 0;
//        alphaProc[a] = 0;
//        for(int i = 0; i < srcImg->width(); i++)
//        {
//            for(int j = 0; j < srcImg->height(); j++)
//            {
//                rgbSrc = srcImg->pixel(i, j);
//                outSrc = 0.3*qRed(rgbSrc)+0.59*qGreen(rgbSrc)+0.11*qBlue(rgbSrc);

//                rgbProc = proccessedImg->pixel(i, j);
//                outProc = 0.3*qRed(rgbProc)+0.59*qGreen(rgbProc)+0.11*qBlue(rgbProc);
//                if (outSrc == a)
//                {
//                    alphaSrc[a]++;
//                }
//                if (outProc == a)
//                {
//                    alphaProc[a]++;
//                }
//            }
//        }
//    }

//    int maxAlpha = 0;
//    int maxSrc = findMax(alphaSrc, 256);
//    int maxProc = findMax(alphaProc, 256);

//    if (maxSrc <= maxProc)
//    {
//        maxAlpha = maxProc;
//    }
//    else
//    {
//        maxAlpha = maxSrc;
//    }

//    int height = 200;
//    int width = 256;

//    double scale = ((double)height) / maxAlpha;

//    QImage *histSrc = new QImage(width, height, QImage::Format_ARGB32);
//    QImage *histProc = new QImage(width, height, QImage::Format_ARGB32);

//    for (int i = 0; i < width; i++)
//    {
//        int realScalesrc = (int)(scale * alphaSrc[i]);
//        for (int j = height - 1; j > height - realScalesrc ; j--)
//        {
//            histSrc->setPixel(i, j , qRgb(0, 0, 0));
//        }

//        int realScaleproc = (int)(scale * alphaProc[i]);
//        for (int j = height - 1; j > height - realScaleproc ; j--)
//        {
//            histProc->setPixel(i, j , qRgb(0, 0, 0));
//        }
//    }
//    ui->sourceImgLabel->setPixmap(QPixmap::fromImage(*histSrc));

//    ui->proccessedImgLabel->setPixmap(QPixmap::fromImage(*histProc));




//}

void HistoGram::onHistProccess()
{

    ui->sourceImgLabel->clear();
    ui->proccessedImgLabel->clear();
    ui->proccessedImgLabel->clearMask();
    ui->sourceImgLabel->clearMask();
    ui->sourceImgLabel->update();
    int height = 200;
    int width = 256;

    qDebug() << "show histogram";
    QImage *histSrc = new QImage(width, height, QImage::Format_ARGB32);
    QImage *histProc = new QImage(width, height, QImage::Format_ARGB32);

    QMap<int, int> redSrc, redProc;
    QMap<int, int> greenSrc, greenProc;
    QMap<int, int> blueSrc, blueProc;

    for (int i = 0; i < 256; i++)
    {
        redSrc.insert(i, 0);
    }

    QRgb rgb;
    QRgb rgbProc;
    int out, outProc;
    int intRedProc, intRedSrc;

    for (int i = 0; i < srcImg->width(); i++)
    {
        for (int j = 0; j < srcImg->height(); j++)
        {
            rgb = srcImg->pixel(i, j);
            out = 0.3*qRed(rgb)+0.59*qGreen(rgb)+0.11*qBlue(rgb);
            intRedSrc = redSrc.value(out);
            intRedSrc++;
            redSrc.insert(out, intRedSrc);

            rgbProc = proccessedImg->pixel(i, j);
            outProc = 0.3*qRed(rgbProc)+0.59*qGreen(rgbProc)+0.11*qBlue(rgbProc);
            intRedProc = redProc.value(outProc);
            intRedProc++;
            redProc.insert(outProc, intRedProc);
        }
    }


    QList<int> redSrcValues = redSrc.values();
    QList<int> redProcValues = redProc.values();

    qSort(redSrcValues);
    qSort(redProcValues);
    int maxRedSrcValue = redSrcValues.at(redSrcValues.size() - 1);
    int maxRedProcValue = redProcValues.at(redProcValues.size() - 1);

    qDebug() << " max src "  << maxRedSrcValue << "  max proc = " << maxRedSrcValue;


    double max = 0;
    if (maxRedProcValue <= maxRedSrcValue)
    {
        max = maxRedSrcValue;
    }
    else
    {
        max = maxRedProcValue;
    }

        double scale =  ((double)height)/max;


    for (int alpha = 0; alpha < 256; alpha++)
    {

        int red = redSrc.value(alpha);
        int rowHeight = (int) (scale * red);
        qDebug() << rowHeight;
        for (int i = height - rowHeight; i < height; i++)
        {
            histSrc->setPixel(alpha, i, qRgb(0, 0, 0));
        }

        red = redProc.value(alpha);
        rowHeight = (int) (scale * red);
        for (int i = height - rowHeight; i < height; i++)
        {
            histProc->setPixel(alpha, i, qRgb(0, 0, 0));
        }

    }

    ui->sourceImgLabel->setPixmap(QPixmap::fromImage(*histSrc));
    ui->proccessedImgLabel->setPixmap(QPixmap::fromImage(*histProc));

    free(histProc);
    free(histSrc);
}



