#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "noice.h"
#include "borders.h"
#include "fsf.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    connect(ui->actionOpen, SIGNAL(triggered(bool)), this, SLOT(onOpenFile()));
    connect(this, SIGNAL(processImgToGray(QImage*)), this, SLOT(onGrayProcess(QImage*)));
    connect(this, SIGNAL(processImgToBin(QImage*,int)), this, SLOT(onBinProcess(QImage*, int)));
    connect(ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(onSliderChanged(int)));
    connect(ui->actionNoise_2, SIGNAL(triggered(bool)), this, SLOT(noise()));
    connect(ui->actionBorders, SIGNAL(triggered(bool)), this, SLOT(borders()));
    connect(ui->actionFrFilter, SIGNAL(triggered(bool)), this, SLOT(frfilter()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::processImage(QString fname)
{

    srcImage = new QImage(fname);
    grayImage = new QImage(srcImage->width(), srcImage->height(), QImage::Format_ARGB32);
    binImage = new QImage(srcImage->width(), srcImage->height(), QImage::Format_ARGB32);
    ui->label_pic->setPixmap(QPixmap::fromImage(*srcImage));


}

void MainWindow::runBin(QImage *img, int threshold)
{
    emit processImgToBin(img, threshold);
}

void MainWindow::onOpenFile()
{
    fname = QFileDialog::getOpenFileName(this, tr("Open image"), "",
                                         tr("All Files (*)"));

    processImage(fname);

    emit processImgToGray(srcImage);
}


void MainWindow::onGrayProcess(QImage *img)
{
    int y_out;
    QRgb rgb, rgb_out;
    for(int i = 0; i < img->width(); i++)
    {
        for(int j=0; j < img->height(); j++)
        {
            rgb = img->pixel(i,j);
            y_out = 0.3*qRed(rgb)+0.59*qGreen(rgb)+0.11*qBlue(rgb);
            rgb_out = qRgb(y_out, y_out, y_out);
            grayImage->setPixel(i, j, rgb_out);
        }
    }
    ui->label_pic1->setPixmap(QPixmap::fromImage(*grayImage));
    emit processImgToBin(grayImage, 90);
}

void MainWindow::onBinProcess(QImage *img, int threshold)
{

    QRgb rgb, rgbOut;
    int y_out;
    for(int i=0; i < img->width(); i++)
    {
        for(int j=0; j < img->height(); j++)
        {
            rgb = img->pixel(i,j);
            y_out = qRed(rgb);
            if(y_out <= threshold)
                rgbOut = qRgb(0, 0, 0);
            if(y_out > threshold)
                rgbOut = qRgb(255,255,255);

            binImage->setPixel(i, j, rgbOut);
        }
    }
    ui->label_pic2->setPixmap(QPixmap::fromImage(*binImage));

}

void MainWindow::onSliderChanged(int threshold)
{
    emit processImgToBin(srcImage, threshold);
}

void MainWindow::noise()
{
    Noice noise;
    noise.setModal(true);
    noise.setImage(srcImage);
    noise.exec();
}

void MainWindow::borders()
{
    Borders borders;
    borders.setModal(true);
    borders.setImage(srcImage);
    borders.exec();
}

void MainWindow::frfilter()
{
    Fsf fsf;
    fsf.setModal(true);
    fsf.setImage(grayImage);
    fsf.exec();
}
