#include "noice.h"
#include "ui_noice.h"
#include "histogram.h"


Noice::Noice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Noice)
{
    ui->setupUi(this);

    connect(ui->rbSaltAndPepper,SIGNAL(toggled(bool)), this, SLOT(onSaltAndPepperFilter(bool)));
    connect(ui->rbGauss,SIGNAL(toggled(bool)), this, SLOT(onGaussFilter(bool)));
    connect(ui->rbOrigImg, SIGNAL(toggled(bool)), this, SLOT(onOrigImage(bool)));
    connect(ui->processBtn, SIGNAL(clicked(bool)), this, SLOT(onProccess()));
    connect(ui->pbHistogram, SIGNAL(clicked(bool)), this, SLOT(onHistogram()));
}

Noice::~Noice()
{
    delete ui;
}

void Noice::setImage(QImage *img)
{

    this->img = img;
    ui->imgLabel->setPixmap(QPixmap::fromImage(*img));
}

int Noice::checkColor(int color)
{
    if (color < 0)
        color = 0;
    if (color > 255)
        color = 255;
    return color;
}

FilterType Noice::getFt()
{

    if (ui->lfFilter->isChecked())
    {
        return LowFrequency;
    }
    else if (ui->hfFilter->isChecked())
    {
        return HighFrequency;
    }
    else if (ui->rhombFilter->isChecked())
    {
        return Rhomb;
    }
    else if (ui->crossFilter->isChecked())
    {
        return Cross;
    }
}

MaskSize Noice::getMs()
{
    if (ui->rb3x3->isChecked())
    {
        return X3;
    }
    else if (ui->rb5x5->isChecked())
    {
        return X5;
    }
    else if (ui->rb7x7->isChecked())
    {
        return X7;
    }
}

void Noice::filterImage(QImage *image, FilterMask *mask)
{
    int mSize = mask->getSize();

    int imgHeight = image->height();
    int imgWidth = image->width();
    int coeff = mask->getCoeff();
    int k = (mSize - 1) /2;


    filteredImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);

    for (int i = k; i < imgWidth - k; i++)
    {
        for (int j = k; j < imgHeight - k; j++)
        {
            double newValR = 0;
            double newValG = 0;
            double newValB = 0;




            for (int a = 0 ; a < mSize; a++)
            {

                for (int b = 0; b < mSize; b++)
                {

                    double el = mask->getEl(b, a);



                    newValR += el * qRed(image->pixel(i - k + a, j - k + b));
                    newValG += el * qGreen(image->pixel(i - k + a, j - k + b));
                    newValB += el * qBlue(image->pixel(i - k + a, j - k + b));

                }

            }


            newValR = newValR / coeff;
            newValG = newValG / coeff;
            newValB = newValB / coeff;


            newValR = checkColor((int)newValR);
            newValG = checkColor((int)newValG);
            newValB = checkColor((int)newValB);



            filteredImage->setPixel(i, j, qRgb((int)newValR, (int)newValG, (int)newValB));

        }
    }

    ui->outImgLabel->setPixmap(QPixmap::fromImage(*filteredImage));

}

void Noice::nonLinearFilterRhomb(QImage *image, MaskSize mSize)
{
    int imgHeight = image->height();
    int imgWidth = image->width();
    int k = (mSize - 1) / 2;


    filteredImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);


    for(int i = k; i < imgWidth - k; i++)
    {
        for (int j = k; j < imgHeight - k; j++)
        {
            QList<int> rList;
            QList<int> gList;
            QList<int> bList;


            for(int aa = 0; aa < k; aa++)
            {
                for (int b = j - aa; b <= j + aa; b++)
                {
                    rList.append(qRed(image->pixel(i - k + aa, b)));
                    gList.append(qGreen(image->pixel(i - k + aa, b)));
                    bList.append(qBlue(image->pixel(i - k + aa, b)));
                }
                for (int b = j - aa; b <= j + aa; b++)
                {
                    rList.append(qRed(image->pixel(i + k - aa, b)));
                    gList.append(qGreen(image->pixel(i + k - aa, b)));
                    bList.append(qBlue(image->pixel(i + k - aa, b)));
                }

            }

            for(int b = j - k; b <= j + k; b++)
            {
                rList.append(qRed(image->pixel(i, b)));
                gList.append(qGreen(image->pixel(i, b)));
                bList.append(qBlue(image->pixel(i, b)));
            }


            qSort(rList);
            qSort(gList);
            qSort(bList);

            int rVal = rList.at((rList.size()/2) + 1);
            int gVal = gList.at((gList.size()/2) + 1);
            int bVal = bList.at((bList.size()/2) + 1);

            filteredImage->setPixel(i, j, qRgb(rVal, gVal, bVal));

        }
    }

    ui->outImgLabel->setPixmap(QPixmap::fromImage(*filteredImage));
}

void Noice::nonLinearFilterCross(QImage *image, MaskSize mSize)
{
    int imgHeight = image->height();
    int imgWidth = image->width();
    int k = (mSize - 1) /2;

    filteredImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);

    for (int i = k; i < imgWidth - k; i++)
    {
        for(int j = k; j < imgHeight - k; j++)
        {
            QList<int> rList;
            QList<int> gList;
            QList<int> bList;
            for(int a = 0; a < mSize; a++)
            {
                for(int b = 0; b < mSize; b++)
                {
                    if (((i - k + a) == i) || ((j - k + b) == j))
                    {
                        rList.append(qRed(image->pixel(i - k + a, j - k + b)));
                        gList.append(qGreen(image->pixel(i - k + a, j - k + b)));
                        bList.append(qBlue(image->pixel(i - k + a, j - k + b)));
                    }

                }
            }

            qSort(rList);
            qSort(gList);
            qSort(bList);

            int rVal = rList.at((rList.size()/2) + 1);
            int gVal = gList.at((gList.size()/2) + 1);
            int bVal = bList.at((bList.size()/2) + 1);

            filteredImage->setPixel(i, j, qRgb(rVal, gVal, bVal));

        }
    }

    ui->outImgLabel->setPixmap(QPixmap::fromImage(*filteredImage));
}

void Noice::onSaltAndPepperFilter(bool flag)
{
    if (!flag)
    {
        ui->imgLabel->setPixmap(QPixmap::fromImage(*img));
        return;
    }
    picRes = img->copy(0,0,img->width(),img->height());
    int salpix = img->height() * img->width() *0.02;
    QTime time(0, 0, 0);

    qsrand(time.secsTo(QTime::currentTime()));
    for(int i = 0; i < salpix; i++)
    {
        picRes.setPixel(qrand() % img->width(), qrand() % img->height(), qRgb(255, 255, 255));
    }


    for(int i = 0; i < salpix; i++)
    {
        picRes.setPixel(qrand() % img->width(), qrand() % img->height(), qRgb(0, 0, 0));
    }

    ui->imgLabel->setPixmap(QPixmap::fromImage(picRes));


}

void Noice::onGaussFilter(bool flag)
{
    if (!flag)
    {
        ui->imgLabel->setPixmap(QPixmap::fromImage(*img));
        return;
    }

    picRes = img->copy(0,0,img->width(),img->height());

    QTime time(0, 0, 0);
    qsrand(time.secsTo(QTime::currentTime()));
    QRgb rgb;
    int randR, randG, randB, outR, outG, outB;

    for(int i = 0; i < img->width(); i++)
        for(int j = 0; j < img->height(); j++)
        {
            rgb = picRes.pixel(i,j);
            randR = (qrand() % 64)-32;
            randG = (qrand() % 64)-32;
            randB = (qrand() % 64)-32;
            outR = qRed(rgb) + randR;
            outG = qGreen(rgb) + randG;
            outB = qBlue(rgb) + randB;
            outR = checkColor(outR);
            outG = checkColor(outG);
            outB = checkColor(outB);
            picRes.setPixel(i, j, qRgb(outR, outG, outB));
        }

    ui->imgLabel->setPixmap(QPixmap::fromImage(picRes));

}

void Noice::onOrigImage(bool flag)
{
    if (!flag)
    {
        ui->imgLabel->setPixmap(QPixmap::fromImage(*img));
        return;
    }

    picRes = img->copy(0,0,img->width(),img->height());
    picRes = *img;

    ui->imgLabel->setPixmap(QPixmap::fromImage(picRes));


}

void Noice::onProccess()
{

    FilterType fType = getFt();
    MaskSize mSize = getMs();

    int imgHeight = picRes.height();
    int imgWidth = picRes.width();

    FilterMask *mask = new FilterMask(fType, mSize);


    filteredImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);
    if (fType == LowFrequency || fType == HighFrequency)
    {
        filterImage(&picRes, mask);
    }
    else if (fType == Rhomb)
    {
        nonLinearFilterRhomb(&picRes, mSize);
    }
    else if (fType == Cross)
    {
        nonLinearFilterCross(&picRes, mSize);
    }
}

void Noice::onHistogram()
{
    HistoGram histoGram;
    histoGram.setSourceImage(img);
    histoGram.setProccessedImage(filteredImage);
    histoGram.exec();
}


