#ifndef BORDERS_H
#define BORDERS_H

#include <QDialog>
#include "filtermask.h"
#include "filtertype.h"
#include "masksize.h"

namespace Ui {
class Borders;
}

class Borders : public QDialog
{
    Q_OBJECT

public:
    explicit Borders(QWidget *parent = 0);
    ~Borders();
    void setImage(QImage *img);

private:
    Ui::Borders *ui;
    QImage *img;
    QImage picRes;
    QImage *filteredImage;
    void filterImage(QImage *image, FilterMask *mask);
    int checkColor(int color);
    FilterType getFt();

private slots:
    void onProcess();
    void onHistogram();
};

#endif // BORDERS_H
