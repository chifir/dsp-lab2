#include "fsf.h"
#include "ui_fsf.h"
#include "histogram.h"

Fsf::Fsf(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Fsf)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked(bool)), this, SLOT(ft()));
    connect(ui->pushButton_2, SIGNAL(clicked(bool)), this, SLOT(uft()));
    // connect(ui->rbLf25, SIGNAL(toggled(bool)), this, SLOT(lfGauss25(bool)));
    connect(ui->pbFilter, SIGNAL(clicked(bool)), this, SLOT(onFilter()));
    connect(ui->pbHistogram, SIGNAL(clicked(bool)), this, SLOT(onHistogram()));
}

Fsf::~Fsf()
{
    delete ui;
}

void Fsf::setImage(QImage *img)
{
    this->img = img;
    ui->inImage->setPixmap(QPixmap::fromImage(*img));

}


void Fsf::fourierTranslation(QImage *img)
{

    int imgWidth = img->width();
    int imgHeight = img->height();

    spectrImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);
    complex<double> picRes;
    complex<double> red;

    complex<double> pic;
    complex<double> tmp;
    int n = imgHeight * imgWidth;

    spectrList = new QList<complex<double>>;

    double value;
    int pix;
    for (int i = 0; i < imgWidth; i++)
    {
        for(int j = 0; j < imgHeight; j++)
        {
            picRes = (0, 0);

            pic = (0, 0);

            value = 0;
            for(int k = 0; k < imgWidth; k++)
            {
                for(int l = 0; l < imgHeight; l++)
                {
                    red = real(qRed(img->pixel(k, l))) * pow(-1, k + l);

                    pic = complex<double>(cos(2 * M_PI *(i * k * imgHeight + j * l * imgWidth)/ n), -1 * sin(2 * M_PI * (i * k * imgHeight + j * l * imgWidth)/ n));
                    tmp = pic*red;
                    picRes += tmp;
                }

            }
            spectrList->append(picRes);

            value = sqrt(pow(picRes.real(), 2) + pow(picRes.imag(), 2));
            value = value / imgHeight;                              // ???????????
            pix = checkColor((int)value);
            spectrImage->setPixel(i, j, qRgb(pix, pix, pix));

        }
        qDebug() << "i = " << i;
    }

    ui->spectrImage->setPixmap(QPixmap::fromImage(*spectrImage));
}

void Fsf::undofourierTranslation(QImage *img)
{

    int imgWidth = img->width();
    int imgHeight = img->height();
    int n = imgHeight * imgWidth;
    complex<double> filteredSpectr[imgWidth][imgHeight];
 //   complex<double> spectr[imgWidth][imgHeight];
    outImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);
    for (int i = 0; i < imgWidth; i++)
    {
        for (int j = 0; j < imgHeight; j++)
        {
            filteredSpectr[i][j] = filetereSpectrList->at(i * imgHeight + j);
           // spectr[i][j] = spectrList->at(i * imgHeight + j);
        }
    }

    complex<double> picRes;
    complex<double> red;

    complex<double> pic;
    complex<double> tmp;

    double value;
    int pix;
    for (int i = 0; i < imgWidth; i++)
    {
        for(int j = 0; j < imgHeight; j++)
        {
            picRes = (0, 0);

            pic = (0, 0);

            value = 0;
            for(int k = 0; k < imgWidth; k++)
            {
                for(int l = 0; l < imgHeight; l++)
                {
               //     red = spectr[k][l];
                    red = filteredSpectr[k][l];// * pow(-1 , (k + l));

                    pic = complex<double>(cos(2 * M_PI * (i * k + j * l) / imgWidth), sin(2 * M_PI * (i * k + j * l) / imgWidth));
                    tmp = pic * red;
                    picRes += tmp;

                }

            }
            complex<double> finishPic(picRes.real() * pow(-1 , (i + j)), picRes.imag());

            value = sqrt(pow(finishPic.real(), 2) + pow(finishPic.imag(), 2));
            value = value / (imgWidth * imgHeight);
            pix = checkColor((int)value);
            outImage->setPixel(i, j, qRgb(pix, pix, pix));

        }
        qDebug() << "i = " << i;
    }

  ui->outImage->setPixmap(QPixmap::fromImage(*outImage));
}

int Fsf::checkColor(int color)
{
    if (color < 0)
        color = 0;
    if (color > 255)
        color = 255;
    return color;
}

//complex<double> Fsf::getLfCoeff(int x, int y, double d)
//{
//    double re;
//    double euDistance = getEuclideanDistance(x, y);
//    re = qExp(-1 * (pow(euDistance, 2)) / (2 * pow(d, 2)));
//    complex<double> coeff(re, 0);
//    return coeff;
//}

double Fsf::getLfCoeff(int x, int y, double d)
{
    double re;
    double euDistance = getEuclideanDistance(x, y);
    re = qExp(-1 * (pow(euDistance, 2)) / (2 * pow(d, 2)));
    return re;
}

double Fsf::getHfCoeff(int x, int y, double d)
//{
//    double re;
//    double euDistance = getEuclideanDistance(x, y);
//    re = 1 - qExp(-1 * (pow(euDistance, 2)) / (2 * pow(d, 2)));
//    return re;

//}
{
    double re;
    double euDistance = getEuclideanDistance(x, y);
    re = 1 / (1 + pow((d / euDistance), 8));
    return re;

}

double Fsf::getEuclideanDistance(int x, int y)
{
    double imgHeight = (double) spectrImage->height()/2;
    double imgWidth = (double) spectrImage->width()/2;

    return sqrt(pow(x - imgWidth, 2) + pow(y - imgHeight, 2));
}

void Fsf::ft()
{
    fourierTranslation(img);
}

void Fsf::uft()
{
    undofourierTranslation(spectrImage);
}

void Fsf::lfGauss(double freqFil)
{
    qDebug() << "hfGauss";
    int imgHeight = spectrImage->height();
    int imgWidth = spectrImage->width();

    filteredSpectrImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);

 //   complex<double> filter[imgWidth][imgHeight];
    double filter[imgWidth][imgHeight];

    for (int i = 0; i < imgWidth; i++)
    {

        for (int j = 0; j < imgHeight; j++)
        {
            filter[i][j] = getLfCoeff(i, j, freqFil);

        }

    }

    complex<double> spectr[imgWidth][imgHeight];

    outImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);

    for (int i = 0; i < imgWidth; i++)
    {
        for (int j = 0; j < imgHeight; j++)
        {
            spectr[i][j] = spectrList->at(i * imgHeight + j);
        }
    }

    // spectrList->clear();
    filetereSpectrList = new QList<complex<double>>;
    for (int i = 0; i < imgWidth; i++)
    {
        for (int j = 0; j < imgHeight; j++)
        {
            int pix = 0;

            complex<double> val(0, 0);

            double value;

            val = spectr[i][j] * filter[i][j];
//            for (int k = 0; k < imgWidth; k++)
//            {
//                val += (spectr[i][k] * filter[k][j]);

//            }
            filetereSpectrList->append(val);

            value = sqrt(pow(val.real(), 2) + pow(val.imag(), 2));

            pix = checkColor((int)value);
            filteredSpectrImage->setPixel(i, j, qRgb(pix, pix, pix));
        }
    }

    ui->filterspectrImage->setPixmap(QPixmap::fromImage(*filteredSpectrImage));

//    complex<double> val;
//    double value;
//    for (int i = 0; i < imgWidth; i++)
//    {
//        for (int j= 0; j < imgHeight; i++)
//        {
//            val = filteredSpectr[i][j];
//            value = sqrt(pow(val.real(), 2) + pow(val.imag(), 2));
//            value = value / (imgHeight * imgWidth);
//            pix = checkColor((int)value);
//            fi->setPixel(i, j, qRgb(pix, pix, pix));

//        }
    //    }
}

void Fsf::hfGauss(double freqFill)
{
    qDebug() << "lfGauss";
    int imgHeight = spectrImage->height();
    int imgWidth = spectrImage->width();

    filteredSpectrImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);

 //   complex<double> filter[imgWidth][imgHeight];
    double filter[imgWidth][imgHeight];

    for (int i = 0; i < imgWidth; i++)
    {

        for (int j = 0; j < imgHeight; j++)
        {
            filter[i][j] = getHfCoeff(i, j, freqFill);

        }

    }

    complex<double> spectr[imgWidth][imgHeight];

    outImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);

    for (int i = 0; i < imgWidth; i++)
    {
        for (int j = 0; j < imgHeight; j++)
        {
            spectr[i][j] = spectrList->at(i * imgHeight + j);
        }
    }

    // spectrList->clear();
    filetereSpectrList = new QList<complex<double>>;
    for (int i = 0; i < imgWidth; i++)
    {
        for (int j = 0; j < imgHeight; j++)
        {
            int pix = 0;

            complex<double> val(0, 0);

            double value;

            val = spectr[i][j] * filter[i][j];
//            for (int k = 0; k < imgWidth; k++)
//            {
//                val += (spectr[i][k] * filter[k][j]);

//            }
            filetereSpectrList->append(val);

            value = sqrt(pow(val.real(), 2) + pow(val.imag(), 2));

            pix = checkColor((int)value);
            filteredSpectrImage->setPixel(i, j, qRgb(pix, pix, pix));
        }
    }

    ui->filterspectrImage->setPixmap(QPixmap::fromImage(*filteredSpectrImage));

//    complex<double> val;
//    double value;
//    for (int i = 0; i < imgWidth; i++)
//    {
//        for (int j= 0; j < imgHeight; i++)
//        {
//            val = filteredSpectr[i][j];
//            value = sqrt(pow(val.real(), 2) + pow(val.imag(), 2));
//            value = value / (imgHeight * imgWidth);
//            pix = checkColor((int)value);
//            fi->setPixel(i, j, qRgb(pix, pix, pix));

//        }
    //    }

}

void Fsf::onFilter()
{
    if (ui->rbLf5->isChecked())
    {
        lfGauss(5);
    }
    else if (ui->rbLf10->isChecked())
    {
        lfGauss(10);
    }
    else if (ui->rbLf25->isChecked())
    {
        lfGauss(25);
    }
    else if (ui->rbHf1->isChecked())
    {
        hfGauss(10);
    }
    else if (ui->rbHf2->isChecked())
    {
        hfGauss(1);
    }
    else if (ui->rbHf3->isChecked())
    {
        hfGauss(0.000001);
    }
}

void Fsf::onHistogram()
{
    HistoGram histoGram;
    histoGram.setSourceImage(img);
    histoGram.setProccessedImage(outImage);
    histoGram.exec();
}
