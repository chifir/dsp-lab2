#include "borders.h"
#include "ui_borders.h"
#include "histogram.h"

Borders::Borders(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Borders)
{
    ui->setupUi(this);
    connect(ui->pbProc, SIGNAL(clicked(bool)), this, SLOT(onProcess()));
    connect(ui->pbHistogram, SIGNAL(clicked(bool)), this, SLOT(onHistogram()));
}

Borders::~Borders()
{
    delete ui;
}

void Borders::setImage(QImage *img)
{
    this->img = img;
    ui->inImage->setPixmap(QPixmap::fromImage(*img));
}

void Borders::filterImage(QImage *image, FilterMask *mask)
{
    int mSize = mask->getSize();

    int imgHeight = image->height();
    int imgWidth = image->width();
    int coeff = mask->getCoeff();
    int k = (mSize - 1) /2;


    filteredImage = new QImage(imgWidth, imgHeight, QImage::Format_ARGB32);

    for (int i = k; i < imgWidth - k; i++)
    {
        for (int j = k; j < imgHeight - k; j++)
        {
            double newValR = 0;
            double newValG = 0;
            double newValB = 0;



            for (int a = 0 ; a < mSize; a++)
            {

                for (int b = 0; b < mSize; b++)
                {

                    double el = mask->getEl(b,a);



                    newValR += el * qRed(image->pixel(i - k + a, j - k + b));
                    newValG += el * qGreen(image->pixel(i - k + a, j - k + b));
                    newValB += el * qBlue(image->pixel(i - k + a, j - k + b));

                }

            }

            newValR = newValR / coeff;
            newValG = newValG / coeff;
            newValB = newValB / coeff;

            newValR = checkColor((int)newValR);
            newValG = checkColor((int)newValG);
            newValB = checkColor((int)newValB);



            filteredImage->setPixel(i, j, qRgb((int)newValR, (int)newValG, (int)newValB));
        }
    }

    ui->outImage->setPixmap(QPixmap::fromImage(*filteredImage));

}

int Borders::checkColor(int color)
{
    {
        if (color < 0)
            color = 0;
        if (color > 255)
            color = 255;
        return color;
    }

}

FilterType Borders::getFt()
{
    if (ui->rbHoriz->isChecked())
    {
        return Horizontal;
    }
    else if (ui->rbVert->isChecked())
    {
        return Vertical;
    }
    else if (ui->rbp45->isChecked())
    {
        return Positive45;
    }
    else if (ui->rbn45->isChecked())
    {
        return Negative45;
    }
    else if (ui->rbLaplas->isChecked())
    {
        return Laplase;
    }
    else if (ui->rbPrevvite->isChecked())
    {
        return Prevvite;
    }
}

void Borders::onProcess()
{
    FilterType fType = getFt();
    MaskSize mSize = X3;

    FilterMask *mask = new FilterMask(fType, mSize);

    filterImage(img, mask);
}

void Borders::onHistogram()
{
    HistoGram histoGram;
    histoGram.setSourceImage(img);
    histoGram.setProccessedImage(filteredImage);
    histoGram.exec();
}
