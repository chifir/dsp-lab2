#ifndef MASKSIZE_H
#define MASKSIZE_H

enum MaskSize
{
    X3 = 3,
    X5 = 5,
    X7 = 7
};

#endif // MASKSIZE_H
