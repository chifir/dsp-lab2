#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QFileDialog>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QWidget *noiseWdgt;
    QString fname;
    void processImage(QString fname);
    void runBin(QImage *img, int threshold);
    QImage *srcImage;
    QImage *grayImage;
    QImage *binImage;

private slots:
    void onOpenFile();
    void onGrayProcess(QImage *img);
    void onBinProcess(QImage *img, int threshold);
    void onSliderChanged(int treshold);
    void noise();
    void borders();
    void frfilter();

signals:
    void processImgToGray(QImage *img);
    void processImgToBin(QImage *img, int threshold);

};

#endif // MAINWINDOW_H
