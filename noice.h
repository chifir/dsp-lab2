#ifndef NOICE_H
#define NOICE_H

#include <QDialog>
#include <QDebug>
#include <QTime>

#include "filtertype.h"
#include "filtermask.h"
#include "masksize.h"
#include <QDebug>

namespace Ui {
class Noice;
}

class Noice : public QDialog
{
    Q_OBJECT

public:
    explicit Noice(QWidget *parent = 0);
    ~Noice();
    void setImage(QImage *img);

    QImage *getFilteredImageRhomb() const;
    void setFilteredImageRhomb(QImage *value);

private:
    Ui::Noice *ui;
    QImage *img;
    QImage picRes;
    QImage *filteredImage;
    int checkColor(int color);
    FilterType getFt();
    MaskSize getMs();
    void filterImage(QImage *image, FilterMask *mask);
    void nonLinearFilterRhomb(QImage *image, MaskSize msize);
    void nonLinearFilterCross(QImage *image, MaskSize msize);

private slots:
    void onSaltAndPepperFilter(bool flag);
    void onGaussFilter(bool flag);
    void onOrigImage(bool flag);
    void onProccess();
    void onHistogram();
};

#endif // NOICE_H
