#include "filtermask.h"

FilterMask::FilterMask(FilterType fType, MaskSize mSize)
{
    this->fType = fType;
    this->mSize = mSize;
}


int FilterMask::getEl(int i, int j)
{
    switch (fType) {
    case LowFrequency:
    {
        switch(mSize)
        {
            case 3: return lf3x3[i][j];
            case 5: return lf5x5[i][j];
            case 7: return lf7x7[i][j];
        };
    }
        break;

    case HighFrequency:
    {
        switch(mSize)
        {
            case 3: return hf3x3[i][j];
            case 5: return hf5x5[i][j];
            case 7: return hf7x7[i][j];
        };
    }
        break;
    case Horizontal:
    {
        return hor3x3[i][j];
    }
        break;
    case Vertical:
    {
        return ver3x3[i][j];
    }
        break;
    case Positive45:
    {
        return p45[i][j];
    }
        break;
    case Negative45:
    {
        return n45[i][j];
    }
        break;
    case Laplase:
    {
        return laplas[i][j];
    }
        break;
    case Prevvite:
    {
        return prevvit[i][j];
    }
        break;
    default:
        break;
    }
}

int FilterMask::getCoeff()
{
    switch (fType) {
    case LowFrequency:
    {
        switch(mSize)
        {
            case 3: return lf3x3Coeff;
            case 5: return lf5x5Coeff;
            case 7: return lf7x7Coeff;
        };
    }
        break;

    case HighFrequency:
    {
        return 1;
    }
        break;
    default:
        return 1;
        break;
    }

}

int FilterMask::getSize()
{
    return (int) mSize;
}
