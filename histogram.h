#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <QDialog>
#include <QDebug>
#include <algorithm>

namespace Ui {
class HistoGram;
}

class HistoGram : public QDialog
{
    Q_OBJECT

public:
    explicit HistoGram(QWidget *parent = 0);
    ~HistoGram();
    void setSourceImage(QImage *srcImg);
    void setProccessedImage(QImage *proccessedImg);

signals:
    void proccessHist();

private:
    Ui::HistoGram *ui;
    QImage *srcImg;
    QImage *proccessedImg;
    int findMax(int *arr, int size);

private slots:
    void onHistProccess();

};

#endif // HISTOGRAM_H
