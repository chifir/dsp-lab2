#ifndef FILTERTYPE_H
#define FILTERTYPE_H

enum FilterType
{
    LowFrequency,
    HighFrequency,
    Rhomb,
    Cross,
    Horizontal,
    Vertical,
    Positive45,
    Negative45,
    Laplase,
    Prevvite
};

#endif // FILTERTYPE_H
