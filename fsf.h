#ifndef FSF_H
#define FSF_H

#include <QDialog>
#include <QtMath>
#include <complex.h>
#include <QDebug>
#include <stdio.h>
#include <iostream>

using namespace  std;

namespace Ui {
class Fsf;
}

class Fsf : public QDialog
{
    Q_OBJECT

public:
    explicit Fsf(QWidget *parent = 0);
    ~Fsf();
    void setImage(QImage *img);

private:
    Ui::Fsf *ui;
    QImage *img;
    QImage *spectrImage;
    QImage *filteredSpectrImage;
    QImage *outImage;
    QList<complex<double>> *spectrList;
    QList<complex<double>> *filetereSpectrList;
    void fourierTranslation(QImage *img);
    void undofourierTranslation(QImage *img);
    int checkColor(int color);

  //  complex<double> getLfCoeff(int x, int y, double d);
    double getLfCoeff(int x, int y, double d);
    double getHfCoeff(int x, int y, double d);
    double getEuclideanDistance(int x, int y);
    void lfGauss(double freqFil);
    void hfGauss(double freqFill);

private slots:
    void ft();
    void uft();
    // void lfGauss25(bool flag);
    void onFilter();
    void onHistogram();
};

#endif // FSF_H
